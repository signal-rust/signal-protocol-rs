use byteorder;
use std;

use byteorder::ByteOrder;

use ::crypto;
use ::protobufs::fingerprint as pb;
use ::utils::*;

use std::fmt::Write;
use std::ops::Index;

//use ::utils::*;
use ::{Error, Result, SerializeCrypto};



/**
 * Binary representation of a local↔remote-fingerprint pair
 *
 * This is mainly intended to be serialized and converted into a QR that can then be scanned
 * (hence the name), deserialized and verified by the communication partner.
 */
#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub struct ScannableFingerprint<'cp> {
	version: u32,
	
	local_stable_identifier: String,
	local_identity_key:      crypto::PublicKey<'cp>,
	
	remote_stable_identifier: String,
	remote_identity_key:      crypto::PublicKey<'cp>,
}

impl<'cp> ScannableFingerprint<'cp> {
	/**
	 * # Parameters
	 * * `version`                  – Fingerprint version (currently always 0)
	 * * `local_stable_identifier`  – Stable identification string (phone number) of the local user
	 * * `local_identity_key`       – Public key of the local user
	 * * `remote_stable_identifier` – Stable identification string (phone number) of the remote user
	 * * `remote_identity_key`      – Public key of the remote user
	 */
	pub fn new(version: u32,
			local_stable_identifier:  String, local_identity_key:  crypto::PublicKey<'cp>,
			remote_stable_identifier: String, remote_identity_key: crypto::PublicKey<'cp>) -> Self {
		ScannableFingerprint {
			version: version,
			
			local_stable_identifier: local_stable_identifier,
			local_identity_key:      local_identity_key,
			
			remote_stable_identifier: remote_stable_identifier,
			remote_identity_key:      remote_identity_key,
		}
	}
	
	
	/**
	 * A stable identification string uniquely belonging to the local user
	 *
	 * Usually a phone number is used for this.
	 */
	#[inline]
	pub fn local_stable_identifier(&self) -> &str {
		&self.local_stable_identifier
	}
	
	/**
	 * The public key belonging to the local user
	 */
	#[inline]
	pub fn local_identity_key(&self) -> &crypto::PublicKey<'cp> {
		&self.local_identity_key
	}
	
	/**
	 * A stable identification string uniquely belonging to the remote user
	 *
	 * Usually a phone number is used for this.
	 */
	#[inline]
	pub fn remote_stable_identifier(&self) -> &str {
		&self.remote_stable_identifier
	}
	
	/**
	 * The public key belonging to the remote user
	 *
	 * Ideally the public key should have been transmitted without the use of any data network.
	 */
	#[inline]
	pub fn remote_identity_key(&self) -> &crypto::PublicKey<'cp> {
		&self.remote_identity_key
	}
	
	
	/**
	 * Check if this fingerprint is the complement of the given fingerprint
	 *
	 * This way it can be verified that the public key of Alice indeed matches the one Bob has been
	 * told it is. Additionally Bob can check whether Alice indeed has his correct public key.
	 *
	 * # Explanation
	 *
	 * If Alice creates a fingerprint of her and Bob and Bob creates a fingerprint of himself and
	 * Alice then these should match, because Alice's identifier and public key should be in the
	 * *local* fields of her fingerprint and in the *remote* fields in Bob's. Bob's identifier and
	 * public key, on the other hand, should be in the *local* fields of his fingerprint and in the
	 * *remote* fields of Alice's.
	 */
	pub fn eq_complementary(&self, other: &Self) -> bool {
		self.version                  == other.version                  &&
		self.local_stable_identifier  == other.remote_stable_identifier &&
		self.local_identity_key       == other.remote_identity_key      &&
		self.remote_stable_identifier == other.local_stable_identifier  &&
		self.remote_identity_key      == other.local_identity_key
	}
}
impl<'cp> SerializeCrypto<'cp, pb::CombinedFingerprint> for ScannableFingerprint<'cp> {
	fn deserialize_protobuf(crypto: &'cp crypto::Provider<'cp>,
	                        mut msg: pb::CombinedFingerprint) -> Result<Self> {
		macro_rules! fingerprint {
			( identifier $fingerprint:expr ) => {
				match String::from_utf8($fingerprint . take_identifier()) {
					Ok(identifier) => identifier,
					Err(err)       => {
						return Err(Error::from(err));
					}
				}
			};
			
			( publicKey $fingerprint:expr ) => {
				try!(crypto::PublicKey::from_key_data(crypto,
					try!(convert_pubkey_bytes($fingerprint . get_publicKey()))
				))
			};
		};
		
		Ok(ScannableFingerprint {
			version: msg.get_version(),
			
			local_stable_identifier: fingerprint!(identifier msg.mut_localFingerprint()),
			local_identity_key:      fingerprint!(publicKey  msg.get_localFingerprint()),
			
			remote_stable_identifier: fingerprint!(identifier msg.mut_remoteFingerprint()),
			remote_identity_key:      fingerprint!(publicKey  msg.get_remoteFingerprint()),
		})
	}
	
	fn serialize_protobuf(&self) -> pb::CombinedFingerprint {
		let mut msg = pb::CombinedFingerprint::new();
		msg.set_version(self.version);
		msg.set_localFingerprint({
			let mut msg = pb::FingerprintData::new();
			msg.set_identifier(self.local_stable_identifier.as_bytes().to_owned());
			msg.set_publicKey((&self.local_identity_key[..]).to_owned());
			msg
		});
		msg.set_remoteFingerprint({
			let mut msg = pb::FingerprintData::new();
			msg.set_identifier(self.remote_stable_identifier.as_bytes().to_owned());
			msg.set_publicKey((&self.remote_identity_key[..]).to_owned());
			msg
		});
		
		msg
	}
}



pub const FINGERPRINT_GENERATOR_VERSION: u32 = 0;

/**
 * Combined storage structure for all local↔remote-fingerprint information
 */
#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub struct Fingerprint<'cp> {
	displayable: DisplayableFingerprint,
	scannable:   ScannableFingerprint<'cp>
}

impl<'cp> Fingerprint<'cp> {
	/**
	 * Create a new `Fingerprint` instance from the parts it contains
	 *
	 * The information for the local and remote parts of fingerprint must match for the displayable
	 * and the scannable fingerprint parts (but this is currently not enforced).
	 */
	pub fn new(displayable: DisplayableFingerprint, scannable: ScannableFingerprint<'cp>) -> Self {
		Fingerprint {
			displayable: displayable,
			scannable:   scannable
		}
	}
	
	/**
	 * Generate a new complete fingerprint object using the given base information
	 *
	 * # Panics
	 *
	 * Panics if the number of SHA512 `iterations` is set to 0.
	 *
	 * # Parameters
	 * * `crypto`                   – Cryptographic provider (for the SHA512 hasher)
	 * * `iterations`               – Number of SHA512 hashing iterations (1024 is recommended)
	 * * `local_stable_identifier`  – Stable identification string (phone number) of the local user
	 * * `local_identity_key`       – Public key of the local user
	 * * `remote_stable_identifier` – Stable identification string (phone number) of the remote user
	 * * `remote_identity_key`      – Public key of the remote user
	 */
	pub fn generate(crypto: &'cp crypto::Provider<'cp>, iterations: usize,
			local_stable_identifier:  String, local_identity_key:  crypto::PublicKey<'cp>,
			remote_stable_identifier: String, remote_identity_key: crypto::PublicKey<'cp>)
				-> Self {
		// Create display strings for the local and remote identifiers
		let local = DisplayableFingerprint::create_string(crypto, iterations,
				&local_stable_identifier,  &local_identity_key
		);
		let remote = DisplayableFingerprint::create_string(crypto, iterations,
				&remote_stable_identifier, &remote_identity_key
		);
		
		// Create fingerprint object and everything it will contain
		let displayable = DisplayableFingerprint::new(&local, &remote);
		let scannable   = ScannableFingerprint::new(FINGERPRINT_GENERATOR_VERSION,
				local_stable_identifier,  local_identity_key,
				remote_stable_identifier, remote_identity_key
		);
		Self::new(displayable, scannable)
	}
	
	/**
	 * Displayable (text) representation of the fingerprint
	 */
	#[inline]
	pub fn displayable(&self) -> &DisplayableFingerprint {
		&self.displayable
	}
	
	/**
	 * Scannable (binary) representation of the fingerprint
	 */
	#[inline]
	pub fn scannable(&self) -> &ScannableFingerprint<'cp> {
		&self.scannable
	}
}



/**
 * Stable textual representation of a local↔remote-fingerprint pair
 */
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct DisplayableFingerprint {
	text: String,
	local_range:  std::ops::Range<usize>,
	remote_range: std::ops::Range<usize>,
}
impl DisplayableFingerprint {
	/**
	 * # Parameters
	 * * `local_fingeprint`   – Textual representation of the local user's key
	 * * `remote_fingerprint` – Textual representation of the remote user's key
	 */
	pub fn new(local_fingerprint: &str, remote_fingerprint: &str) -> Self {
		// Create target string
		let mut text = String::with_capacity(local_fingerprint.len() + remote_fingerprint.len());
		
		let n = text.len();
		if local_fingerprint < remote_fingerprint {
			text.push_str(local_fingerprint);
			text.push_str(remote_fingerprint);
			
			DisplayableFingerprint {
				text: text,
				local_range:  std::ops::Range { start: 0, end: local_fingerprint.len() },
				remote_range: std::ops::Range { start: local_fingerprint.len(), end: n },
			}
		} else {
			text.push_str(remote_fingerprint);
			text.push_str(local_fingerprint);
			
			DisplayableFingerprint {
				text: text,
				local_range:  std::ops::Range { start: remote_fingerprint.len(), end: n },
				remote_range: std::ops::Range { start: 0, end: remote_fingerprint.len() },
			}
		}
	}
	
	
	/**
	 * Create the fingerprint string for a stable identifier and identity key pair
	 */
	pub fn create_string(crypto: &crypto::Provider, iterations: usize,
	                     stable_identifier: &str, identity_key: &crypto::PublicKey) -> String {
		assert!(iterations >= 1);
		
		// Create vector with all data that should be part of the display string
		let data = {
			let mut vec = Vec::with_capacity(2 + 2 * identity_key.len() + stable_identifier.len());
			vec.push(0);
			vec.push(FINGERPRINT_GENERATOR_VERSION as u8);
			vec.extend_from_slice(identity_key.as_ref());
			vec.extend_from_slice(stable_identifier.as_bytes());
			vec.extend_from_slice(identity_key.as_ref());
			vec
		};
		assert_eq!(data.capacity(), data.len());
		
		// Hash display string data using SHA512
		let mut hash = crypto.sha512_digest(data.as_ref());
		
		// Rehash the generated hash for the given number of rounds along with the `identity_key`
		let mut hash_iteration_data = vec![0; (hash.len() + identity_key.len())];
		for _ in 0 .. (iterations - 1) {
			{
				let mut hash_iteration_parts=&mut hash_iteration_data[..].split_at_mut(hash.len());
				hash_iteration_parts.0.copy_from_slice(hash.as_ref());
				hash_iteration_parts.1.copy_from_slice(identity_key.as_ref());
			}
			
			hash = crypto.sha512_digest(&hash_iteration_data[..]);
		};
		
		let mut result = String::with_capacity(30);
		for chunk in hash.as_ref()[0 .. 30].chunks(5) {
			// Reverse chunk byte order and convert to number
			let number = byteorder::NetworkEndian::read_uint(chunk, 5);
			
			// Add number to string as digits
			write!(result, "{:05}", number % 100000).unwrap();
		};
		
		result
	}
	
	
	/**
	 * Get the local fingerprint part
	 */
	pub fn local_fingerprint(&self) -> &str {
		self.text.index(self.local_range.clone())
	}
	
	/**
	 * Get the remote fingerprint part
	 */
	pub fn remote_fingerprint(&self) -> &str {
		self.text.index(self.remote_range.clone())
	}
	
	/**
	 * Get the displayable fingerprint text
	 */
	pub fn text(&self) -> &str {
		self.text.as_ref()
	}
}

#[allow(derive_hash_xor_eq)] // See linked bug report
impl std::hash::Hash for DisplayableFingerprint {
	fn hash<H>(&self, state: &mut H) where H: std::hash::Hasher {
		// Manual derive since `std::ops::Range` does not implement `Hash` itself
		// (see https://github.com/rust-lang/rust/issues/34170)
		state.write(self.text.as_bytes());
		
		state.write_usize(self.local_range.start);
		state.write_usize(self.local_range.end);
		
		state.write_usize(self.remote_range.start);
		state.write_usize(self.remote_range.end);
	}
}