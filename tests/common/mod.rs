#![allow(dead_code)] // Contains shared functionality that is not used by all tests
pub extern crate signal_protocol        as signal;
pub extern crate signal_protocol_crypto as crypto;

pub use self::signal::Serialize;
pub use self::signal::SerializeCrypto;

#[allow(private_in_public)] // It's actually available everywhere anyways
pub use std;



pub struct TestKeyStore {
	store: std::collections::HashMap<String, Vec<u8>>
}
impl TestKeyStore {
	pub fn new() -> Self {
		TestKeyStore {
			store: std::collections::HashMap::new()
		}
	}
}
impl signal::SenderKeyStore for TestKeyStore {
	fn store_sender_key(&mut self, name: &signal::SenderKeyName, record: Vec<u8>) -> signal::Result<()> {
		self.store.insert(name.to_string(), record);
		
		Ok(())
	}
	fn load_sender_key(&self, name: &signal::SenderKeyName) -> signal::Result<Option<&Vec<u8>>> {
		Ok(self.store.get(&name.to_string()))
	}
}


pub fn make_sender_key_store() -> TestKeyStore {
	TestKeyStore::new()
}


pub fn make_crypto_provider<'a>() -> signal::CryptoProvider<'a> {
	signal::CryptoProvider::new(crypto::get_crypto_template())
}