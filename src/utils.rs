use byteorder;

use byteorder::ByteOrder;

use ::{Error, ErrorCode, Result};
use ::crypto;


pub use ::crypto::ObtainProvider;


/**
 * Obtain an unsiged integer full of randomness
 */
pub fn random_generate_u32<'a>(provider: &'a crypto::Provider<'a>) -> u32 {
	// Obtain four bytes of randomness
	let mut buffer: [u8; 4] = [0; 4];
	provider.random(&mut buffer);
	
	// Convert those bytes to an u32
	byteorder::NativeEndian::read_u32(&buffer)
}


/**
 * Check if the given byte slice is exactly 32 bytes long and, if that is in fact the case, copy
 * those bytes into a new array of the same length otherwise return `None`
 */
#[inline]
pub fn slice_to_array_u8_32(slice: &[u8]) -> Option<[u8; 32]> {
	if slice.len() != 32 {
		return None;
	}
	
	let mut array = [0; 32];
	(&mut array[..]).copy_from_slice(slice);
	Some(array)
}

/**
 * Check if the given byte slice is exactly 33 bytes long and, if that is in fact the case, copy
 * those bytes into a new array of the same length otherwise return `None`
 */
#[inline]
pub fn slice_to_array_u8_33(slice: &[u8]) -> Option<[u8; 33]> {
	if slice.len() != 33 {
		return None;
	}
	
	let mut array = [0; 33];
	(&mut array[..]).copy_from_slice(slice);
	Some(array)
}

/**
 * Check if the given byte slice is exactly 64 bytes long and, if that is in fact the case, copy
 * those bytes into a new array of the same length otherwise return `None`
 */
#[inline]
pub fn slice_to_array_u8_64(slice: &[u8]) -> Option<[u8; 64]> {
	if slice.len() != 64 {
		return None;
	}
	
	let mut array = [0; 64];
	(&mut array[..]).copy_from_slice(slice);
	Some(array)
}


#[inline]
pub fn convert_seed_bytes(bytes: &[u8]) -> Result<[u8; 32]> {
	match slice_to_array_u8_32(bytes) {
		Some(seed) => Ok(seed),
		None       => Err(Error::new(ErrorCode::InvalidMessage,
		                             Some("Seed data must be exactly 32 bytes long")
		              ))
	}
}

#[inline]
pub fn convert_pubkey_bytes(bytes: &[u8]) -> Result<[u8; 33]> {
	match slice_to_array_u8_33(bytes) {
		Some(seed) => Ok(seed),
		None       => Err(Error::new(ErrorCode::InvalidMessage,
		                             Some("Public key data must be exactly 33 bytes long")
		              ))
	}
}

#[inline]
pub fn convert_privkey_bytes(bytes: &mut [u8]) -> Result<Box<[u8; 32]>> {
	// Length check
	if bytes.len() != 32 {
		let message = "Private key data must be exactly 32 bytes long";
		return Err(Error::new(ErrorCode::InvalidMessage, Some(message)));
	}
	
	// Copy the actual private key bytes
	let mut array = Box::new([0; 32]);
	(&mut array[..]).copy_from_slice(bytes);
	
	// Zero out the bytes in the previous private key storage (a small defense)
	bytes.copy_from_slice(&[0; 32]);
	
	Ok(array)
}

#[inline]
pub fn convert_chain_key_bytes(bytes: &[u8]) -> Result<[u8; 32]> {
	match slice_to_array_u8_32(bytes) {
		Some(seed) => Ok(seed),
		None       => Err(Error::new(ErrorCode::InvalidMessage,
		                             Some("Chain key data must be exactly 32 bytes long")
		              ))
	}
}

#[inline]
pub fn convert_signature_bytes(bytes: &[u8]) -> Result<[u8; 64]> {
	match slice_to_array_u8_64(bytes) {
		Some(seed) => Ok(seed),
		None       => Err(Error::new(ErrorCode::InvalidMessage,
		                             Some("Signature data must be exactly 64 bytes long")
		              ))
	}
}
