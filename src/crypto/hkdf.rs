use ::crypto;


/**
 * Struct implementing a HMAC-based key derivation function
 *
 * This allows weak key material to be converted into cryptographically stronger key data.
 */
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Context<'cp> {
	crypto: &'cp crypto::Provider<'cp>,
	
	iteration_start_offset: u8
}
impl<'cp> Context<'cp> {
	/**
	 * Create a new HKDF context for the given signal message version and using the HMAC functions
	 * of the given crypto provider
	 *
	 * # Panics
	 *
	 * The message version must be a valid Signal version number: Only version numbers `2` and `3`
	 * currently exist.
	 */
	pub fn new(crypto: &'cp crypto::Provider<'cp>, message_version: u32) -> Self {
		Context { crypto: crypto,
			iteration_start_offset: match message_version {
				2 => 0,
				3 => 1,
				_ => unreachable!()
			}
		}
	}
	
	/**
	 * Find the HMAC-SHA256 hash for the given `input_key_material` and the given `salt`
	 */
	pub fn extract(&self, input_key_material: &[u8], salt: &[u8]) -> [u8; 32] {
		let mut hmac_sha256_generator = self.crypto.hmac_sha256_new(salt);
		hmac_sha256_generator.input(input_key_material);
		hmac_sha256_generator.result()
	}
	
	/**
	 * Create a continuous stream of pseudo random data bytes based on the given `prk`
	 * (pseudo random key) and the optional extra context `info` string
	 */
	pub fn expand(&self, output: &mut [u8], prk: &[u8; 32], info: Option<&[u8]>) {
		let mut prev_step: Option<[u8; 32]> = None;
		
		for (idx, chunk) in output.chunks_mut(32).enumerate() {
			let mut hmac_sha256_generator = self.crypto.hmac_sha256_new(prk);
			
			// Add previous generated data packet to HMAC generator
			prev_step.as_ref().map(|step| {
				hmac_sha256_generator.input(step);
			});
			
			// Add info string to HMAC generator
			info.as_ref().map(|info| {
				hmac_sha256_generator.input(info);
			});
			
			// Add current iteration number
			hmac_sha256_generator.input(&[(idx as u8 + self.iteration_start_offset) as u8]);
			
			// Generate next HMAC data packet and store a copy of it
			let step = hmac_sha256_generator.result();
			let size = chunk.len();
			chunk.copy_from_slice(&step[0 .. size]);
			prev_step = Some(step);
		}
	}
	
	/**
	 * Create a continuous stream of pseudo random data bytes based on the given
	 * `input_key_material` and `salt`, as well as the optional extra context `info` string
	 *
	 * This is a combination of the previous two functions.
	 */
	pub fn derive_secrets(&self, output: &mut [u8], input_key_material: &[u8], salt: &[u8],
	                      info: Option<&[u8]>) {
		let prk = self.extract(input_key_material, salt);
		self.expand(output, &prk, info);
	}
}
impl<'cp> crypto::ObtainProvider<'cp> for Context<'cp> {
	fn get_crypto_provider(&self) -> &'cp crypto::Provider<'cp> {
		self.crypto
	}
}