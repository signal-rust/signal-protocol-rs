mod common;
use common::*;


const GROUP_SENDER: &'static signal::SenderKeyName<'static> = &signal::SenderKeyName {
	group_id: "nihilist history reading group",
	sender: signal::Address {
		name:      "+14150001111",
		device_id: 1
	}
};


fn populate_sender_key_stores<'cp>(crypto: &'cp signal::CryptoProvider<'cp>,
                                   alice_senders: &mut signal::SenderKeyStore,
                                   bob_senders:   &mut signal::SenderKeyStore,
                                   deliver:       bool) {
	// Create the session builders
	let mut alice_session_builder = signal::GroupSessionBuilder::new(crypto, alice_senders);
	let mut bob_session_builder   = signal::GroupSessionBuilder::new(crypto, bob_senders);
	
	// Create the sender key distribution message for Alice
	let message = alice_session_builder.create_session_message(GROUP_SENDER).unwrap();
	
	// Option to drop the message somewhere before it reaches Bob
	if deliver {
		// Add the message at Bob's place and it to his key store
		bob_session_builder.process_session_message(GROUP_SENDER, &message).unwrap();
	}
}


#[test]
fn test_no_session() {
	let crypto = make_crypto_provider();
	
	// Create the test key data storages
	let mut alice_senders = make_sender_key_store();
	let mut bob_senders   = make_sender_key_store();
	
	// Try to add Alice to Bob's key store but drop the message along the way
	populate_sender_key_stores(&crypto, &mut alice_senders, &mut bob_senders, false);
	
	// Create the group ciphers?
	let mut alice_group_cipher = signal::GroupCipher::new(&crypto, &mut alice_senders, GROUP_SENDER);
	let mut bob_group_cipher   = signal::GroupCipher::new(&crypto, &mut bob_senders,   GROUP_SENDER);
	
	// Encrypt a test message from Alice
	let alice_plaintext = "smert ze smert";
	let alice_message   = alice_group_cipher.encrypt(alice_plaintext.as_bytes()).unwrap();
	
	// Attempt to have Bob decrypt the message at his place
	assert_eq!(bob_group_cipher.decrypt(&alice_message).unwrap_err().code(), signal::ErrorCode::NoSession);
}

#[test]
fn test_basic_encrypt_decrypt() {
	let crypto = make_crypto_provider();
	
	// Create the test key data storages
	let mut alice_senders = make_sender_key_store();
	let mut bob_senders   = make_sender_key_store();
	
	// Add Alice to Bob's key store
	populate_sender_key_stores(&crypto, &mut alice_senders, &mut bob_senders, true);
	
	// Create the group ciphers?
	let mut alice_group_cipher = signal::GroupCipher::new(&crypto, &mut alice_senders, GROUP_SENDER);
	let mut bob_group_cipher   = signal::GroupCipher::new(&crypto, &mut bob_senders,   GROUP_SENDER);
	
	// Encrypt a test message from Alice
	let alice_plaintext = "farm equator parent student";
	let alice_message   = alice_group_cipher.encrypt(alice_plaintext.as_bytes()).unwrap();
	
	// Attempt to have Bob decrypt the message at his place
	let plaintext = String::from_utf8(bob_group_cipher.decrypt(&alice_message).unwrap()).unwrap();
	assert_eq!(&plaintext[..], alice_plaintext);
}

#[test]
fn test_basic_ratchet() {
	let crypto = make_crypto_provider();
	
	// Create the test key data storages
	let mut alice_senders = make_sender_key_store();
	let mut bob_senders   = make_sender_key_store();
	
	// Add Alice to Bob's key store
	populate_sender_key_stores(&crypto, &mut alice_senders, &mut bob_senders, true);
	
	// Create the group ciphers for 
	let mut alice_group_cipher = signal::GroupCipher::new(&crypto, &mut alice_senders, GROUP_SENDER);
	let mut bob_group_cipher   = signal::GroupCipher::new(&crypto, &mut bob_senders,   GROUP_SENDER);
	
	// Encrypt several test messages from Alice
	let alice_plaintext1 = "good positive children work"; // <- By password generator :-)
	let alice_plaintext2 = "loss sit physical pressure";
	let alice_plaintext3 = "stretch origin fun task";
	let alice_message1 = alice_group_cipher.encrypt(alice_plaintext1.as_bytes()).unwrap();
	let alice_message2 = alice_group_cipher.encrypt(alice_plaintext2.as_bytes()).unwrap();
	let alice_message3 = alice_group_cipher.encrypt(alice_plaintext3.as_bytes()).unwrap();
	
	// Attempt to have Bob decrypt the first message at his place
	let plaintext1 = String::from_utf8(bob_group_cipher.decrypt(&alice_message1).unwrap()).unwrap();
	assert_eq!(&plaintext1[..], alice_plaintext1);
	
	// Attempt to have Bob decrypt the same message again
	assert_eq!(bob_group_cipher.decrypt(&alice_message1).unwrap_err().code(), signal::ErrorCode::DuplicateMessage);
	
	// Check remaining messages
	let plaintext2 = String::from_utf8(bob_group_cipher.decrypt(&alice_message2).unwrap()).unwrap();
	assert_eq!(&plaintext2[..], alice_plaintext2);
	
	let plaintext3 = String::from_utf8(bob_group_cipher.decrypt(&alice_message3).unwrap()).unwrap();
	assert_eq!(&plaintext3[..], alice_plaintext3);
}


#[test]
fn test_late_join() {
	let crypto = make_crypto_provider();
	
	// Create the test key data storages
	let mut alice_senders = make_sender_key_store();
	let mut bob_senders   = make_sender_key_store();
	
	// Try to add Alice to Bob's key store – unfortunately Bob didn't join the group yet and drops
	//                                       the message
	populate_sender_key_stores(&crypto, &mut alice_senders, &mut bob_senders, false);
	// Pretend that this was sent to several other people as well here…
	
	{
		let mut alice_group_cipher = signal::GroupCipher::new(&crypto, &mut alice_senders, GROUP_SENDER);
		
		// Let Alice encrypt a bunch of messages for her new group
		for _ in 0 .. 100 {
			let plaintext = "rain find principle nose";
			alice_group_cipher.encrypt(plaintext.as_bytes()).unwrap();
		}
	}
	
	// Now Bob joins, Alice sends her key to Bob again
	populate_sender_key_stores(&crypto, &mut alice_senders, &mut bob_senders, true);
	
	let mut alice_group_cipher = signal::GroupCipher::new(&crypto, &mut alice_senders, GROUP_SENDER);
	let mut bob_group_cipher   = signal::GroupCipher::new(&crypto, &mut bob_senders,   GROUP_SENDER);
	
	// Alice welcomes Bob to the group
	let alice_plaintext = "Welcome to the group!";
	let alice_message   = alice_group_cipher.encrypt(alice_plaintext.as_bytes()).unwrap();
	
	let plaintext = String::from_utf8(bob_group_cipher.decrypt(&alice_message).unwrap()).unwrap();
	assert_eq!(&plaintext[..], alice_plaintext);
}

#[test]
fn test_out_of_order() {
	let crypto = make_crypto_provider();
	
	// Create the test key data storages
	let mut alice_senders = make_sender_key_store();
	let mut bob_senders   = make_sender_key_store();
	
	// Add Alice to Bob's key store
	populate_sender_key_stores(&crypto, &mut alice_senders, &mut bob_senders, true);
	
	let mut alice_group_cipher = signal::GroupCipher::new(&crypto, &mut alice_senders, GROUP_SENDER);
	let mut bob_group_cipher   = signal::GroupCipher::new(&crypto, &mut bob_senders,   GROUP_SENDER);
	
	// Generate a batch of 100 messages
	let alice_plaintext = "up the punks";
	let mut messages = Vec::with_capacity(100);
	for _ in 0 .. 100 {
		messages.push(alice_group_cipher.encrypt(alice_plaintext.as_bytes()).unwrap());
	}
	
	// Randomize order of messages
	messages.sort_by(|_, _| {
		let mut rand = [0; 1];
		crypto.random(&mut rand);
		
		rand[0].cmp(&128)
	});
	
	// Let Bob decode the messages
	for message in messages {
		let plaintext = String::from_utf8(bob_group_cipher.decrypt(&message).unwrap()).unwrap();
		assert_eq!(&plaintext[..], alice_plaintext);
	}
}

#[test]
fn test_encrypt_no_session() {
	let crypto = make_crypto_provider();
	
	// Create data storage for Alice
	let mut alice_senders = make_sender_key_store();
	
	// Create group cipher for Alice (no session has been set up yet)
	let mut alice_group_cipher = signal::GroupCipher::new(&crypto, &mut alice_senders, GROUP_SENDER);
	
	// Try to encrypt without a session
	let plaintext = "baby horny car driver";
	assert_eq!(alice_group_cipher.encrypt(plaintext.as_bytes()).unwrap_err().code(), signal::ErrorCode::NoSession);
}

#[test]
fn test_too_far_in_future() {
	let crypto = make_crypto_provider();
	
	// Create the test key data storages
	let mut alice_senders = make_sender_key_store();
	let mut bob_senders   = make_sender_key_store();
	
	// Add Alice to Bob's key store
	populate_sender_key_stores(&crypto, &mut alice_senders, &mut bob_senders, true);
	
	let mut alice_group_cipher = signal::GroupCipher::new(&crypto, &mut alice_senders, GROUP_SENDER);
	let mut bob_group_cipher   = signal::GroupCipher::new(&crypto, &mut bob_senders,   GROUP_SENDER);
	
	// Have Alice encrypt a batch of 2001 messages
	for _ in 0 .. 2001 {
		let plaintext = "down the road";
		alice_group_cipher.encrypt(plaintext.as_bytes()).unwrap();
	}
	
	// Have Alice encrypt another message that is too far ahead of Bob
	let alice_plaintext = "net gonna werk";
	let message = alice_group_cipher.encrypt(alice_plaintext.as_bytes()).unwrap();
	
	// Decrypt Alice's last message at Bob's place and observe it being rejected
	assert_eq!(bob_group_cipher.decrypt(&message).unwrap_err().code(), signal::ErrorCode::InvalidMessage);
}

#[test]
fn test_message_key_limit() {
	let crypto = make_crypto_provider();
	
	// Create the test key data storages
	let mut alice_senders = make_sender_key_store();
	let mut bob_senders   = make_sender_key_store();
	
	// Add Alice to Bob's key store
	populate_sender_key_stores(&crypto, &mut alice_senders, &mut bob_senders, true);
	
	let mut alice_group_cipher = signal::GroupCipher::new(&crypto, &mut alice_senders, GROUP_SENDER);
	let mut bob_group_cipher   = signal::GroupCipher::new(&crypto, &mut bob_senders,   GROUP_SENDER);
	
	// Pre-create a large number of messages from Alice
	let alice_plaintext = "there she goes";
	let mut messages = Vec::with_capacity(2010);
	for _ in 0 .. 2010 {
		messages.push(alice_group_cipher.encrypt(alice_plaintext.as_bytes()).unwrap());
	}
	
	// Try decrypting in-flight message 1000
	assert_eq!(String::from_utf8(bob_group_cipher.decrypt(&messages[1000]).unwrap()).unwrap(), alice_plaintext);
	
	// Try decrypting in-flight message 2009
	assert_eq!(String::from_utf8(bob_group_cipher.decrypt(&messages[2009]).unwrap()).unwrap(), alice_plaintext);
	
	// Try decrypting in-flight message 0, which should fail
	assert_eq!(bob_group_cipher.decrypt(&messages[0]).unwrap_err().code(), signal::ErrorCode::DuplicateMessage);
}