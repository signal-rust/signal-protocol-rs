use ::{Cipher, Error, ErrorCode, Result, Serialize, SerializeCrypto};
use ::crypto;
use ::keys;
use ::protocol;


/**
 * Helper that generates the messages that let other people in a group know your public key
 */
#[allow(missing_debug_implementations)]
pub struct GroupSessionBuilder<'cp, 'a> {
	crypto:    &'cp crypto::Provider<'cp>,
	key_store: &'a mut keys::SenderKeyStore,
}
impl<'cp, 'a> GroupSessionBuilder<'cp, 'a> {
	/**
	 * # Parameters
	 * * `key_store` – The sender key storage object
	 */
	pub fn new(crypto: &'cp crypto::Provider<'cp>, key_store: &'a mut keys::SenderKeyStore) -> Self {
		GroupSessionBuilder {
			crypto:    crypto,
			key_store: key_store
		}
	}
	
	
	/**
	 * Create a new `SenderKeyDistributionMessage` that can be used to inform another person of our
	 * public key without meeting in person
	 *
	 * A service provider can, of course, replace this with a public key of and then read all
	 * messages sent from receivers of this message (Bob) to us (Alice). Therefor it is recommended
	 * to perform in-person verification (using `fingerprint::Fingerprint` for instance), before
	 * transmitting any sensitive data.
	 *
	 * TODO: Revisit this after implementing the main session support… (this not the place where
	 *       the public key is actually exchanged)
	 *
	 * # Errors
	 *
	 * Fails if the `SenderKeyStore` returned an error or if the data it returned was somehow
	 * corrupted. The given `SenderKeyName` *does not* have to exist in the key store however.
	 */
	pub fn create_session_message(&mut self, key_name: &keys::SenderKeyName) -> Result<Vec<u8>> {
		let mut record = match try!(self.key_store.load_sender_key(key_name)) {
			Some(data) => try!(keys::SenderKeyRecord::deserialize(self.crypto, &data[..])),
			None       => keys::SenderKeyRecord::new(self.crypto),
		};
		
		if record.is_empty() {
			// Add a new sender key record state if the record is currently empty
			record.set_key_state(keys::SenderKeyState::generate(self.crypto));
			try!(self.key_store.store_sender_key(key_name, record.serialize()));
		}
		
		let state = record.get_key_state().expect("Record cannot be empty anymore");
		
		// Generate and serialize the message text
		let message = protocol::SenderKeyDistributionMessage::new(
				state.key_id(),
				state.chain_key.iteration(),
				*state.chain_key.seed(),
				state.signing_public_key().clone()
		);
		Ok(message.serialize())
	}
	
	
	/**
	 * Parse a received `SenderKeyDistributionMessage` for the given group `SenderKeyName` and add
	 * it's key material to the `SenderKeyStore`
	 *
	 * # Errors
	 *
	 * Fails if the `SenderKeyStore` returned an error or if the data it returned was somehow
	 * corrupted. The given `SenderKeyName` *does not* have to exist in the key store however.
	 * Also the provided message `data` must resemble a valid `SenderKeyDistributionMessage`.
	 */
	pub fn process_session_message(&mut self, sender_key_name: &keys::SenderKeyName, data: &[u8])
			-> Result<()> {
		let mut record = match try!(self.key_store.load_sender_key(sender_key_name)) {
			Some(data) => try!(keys::SenderKeyRecord::deserialize(self.crypto, &data[..])),
			None       => keys::SenderKeyRecord::new(self.crypto),
		};
		
		// Try to deserialize the message data
		let message = try!(protocol::SenderKeyDistributionMessage::deserialize(self.crypto, data));
		
		// Store the obtained message key into the key record
		{
			record.add_key_state(
					message.key_id(),
					message.iteration(),
					*message.chain_key_seed(),
					message.signature_key().clone()
			);
		}
		
		// Save the modified key record object
		self.key_store.store_sender_key(sender_key_name, record.serialize())
	}
}


/**
 * Group message encryptor and decryptor
 */
#[allow(missing_debug_implementations)]
pub struct GroupCipher<'cp, 'a, 'b> {
	crypto:           &'cp crypto::Provider<'cp>,
	sender_key_store: &'a mut keys::SenderKeyStore,
	sender_key_name:  &'b keys::SenderKeyName<'b>,
}
impl<'cp, 'a, 'b> GroupCipher<'cp, 'a, 'b> {
	/**
	 * 
	 */
	pub fn new(crypto:           &'cp crypto::Provider<'cp>,
	           sender_key_store: &'a mut keys::SenderKeyStore,
	           sender_key_name:  &'b keys::SenderKeyName<'b>) -> Self {
		GroupCipher {
			crypto:           crypto,
			sender_key_store: sender_key_store,
			sender_key_name:  sender_key_name
		}
	}
	
	
	/**
	 * Encrypt the given plaintext data in a way that only people in the group will be able to read
	 * it
	 *
	 * # Errors
	 *
	 * Fails if the `SenderKeyStore` returned an error or couldn't find the group's `SenderKeyName`
	 * or if the data it returned was somehow corrupted.
	 */
	pub fn encrypt(&mut self, padded_plaintext: &[u8]) -> Result<Vec<u8>> {
		// Locate and deserialize the sender key record using the provided name
		let mut record = match try!(self.sender_key_store.load_sender_key(self.sender_key_name)) {
			Some(data) => try!(keys::SenderKeyRecord::deserialize(self.crypto, &data[..])),
			None       => return Err(Error::new(ErrorCode::NoSession, None))
		};
		
		let message;
		{
			let mut key_state = try!(record.get_key_state_mut()
					.ok_or(Error::new(ErrorCode::InvalidKeyId, None)));
			
			// Encrypt the given plaintext using a key derived from the current chain key
			let message_key = key_state.chain_key.create_message_key();
			let cipher = Cipher::AES_CBC_PKCS5;
			let key    = message_key.cipher_key();
			let iv     = message_key.iv();
			let ciphertext = try!(self.crypto.aes_encrypt(cipher, key, iv, padded_plaintext));
			
			// Advance to next key in the chain key chain
			let chain_key = key_state.chain_key.create_next();
			key_state.chain_key = chain_key;
			
			// Create a signed message for the generated cipher text
			message = protocol::SenderKeyMessage::new(
					key_state.key_id(),
					message_key.iteration(),
					ciphertext,
					key_state.signing_private_key().expect("Record base key must have private key")
			);
		}
		
		// Store the sender key record with the updated chain key value
		try!(self.sender_key_store.store_sender_key(self.sender_key_name, record.serialize()));
		
		Ok(message.serialize())
	}
	
	
	/**
	 * Decrypt the given `SenderKeyMessage´ data that was created by another member of this group
	 *
	 * # Errors
	 *
	 * Fails if the `SenderKeyStore` returned an error or couldn't find the group's `SenderKeyName`
	 * or if the data it returned was somehow corrupted. Also the provided message `data` must
	 * resemble a valid `SenderKeyMessage` and it's signature must be valid.
	 */
	pub fn decrypt(&mut self, data: &[u8]) -> Result<Vec<u8>> {
		// Locate and deserialize the sender key record using the provided name
		let mut record = match try!(self.sender_key_store.load_sender_key(self.sender_key_name)) {
			Some(data) => try!(keys::SenderKeyRecord::deserialize(self.crypto, &data[..])),
			None       => {
				let message = &format!("No sender key for: {}", self.sender_key_name);
				return Err(Error::new(ErrorCode::NoSession, Some(message)));
			}
		};
		
		// Try to deserialize the message data
		let message = try!(protocol::SenderKeyMessage::deserialize(data));
		
		let message_key;
		{
			// Obtain expected public key for verifying the received message
			let mut key_state = try!(record.get_key_state_by_id_mut(message.key_id())
					.ok_or(Error::new(ErrorCode::InvalidKeyId, None)));
			
			// Verify signature on message
			if !message.verify_signature(key_state.signing_public_key()) {
				return Err(Error::new(ErrorCode::InvalidMessage, Some("Invalid signature")));
			}
			
			// Obtain the right decryption key for this message
			message_key = try!(key_state.get_message_key(message.iteration()));
		}
		
		// Store the sender key record with the updated chain key value
		try!(self.sender_key_store.store_sender_key(self.sender_key_name, record.serialize()));
		
		// Decrypt and return the given ciphertext message
		let cipher = Cipher::AES_CBC_PKCS5;
		let key    = message_key.cipher_key();
		let iv     = message_key.iv();
		self.crypto.aes_decrypt(cipher, key, iv, message.ciphertext())
	}
}