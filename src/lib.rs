#![allow(dead_code, unknown_lints)]
#![deny(missing_docs, unused_qualifications, variant_size_differences, fat_ptr_transmutes)]
#![deny(unsafe_code, missing_debug_implementations, missing_copy_implementations)]
#![forbid(future_incompatible, unused_extern_crates)]

/*!
 * Signal protocol implementation
 *
 * Top-level documentation has not been written yet. Please see the structually similar
 * [C library](https://github.com/WhisperSystems/libsignal-protocol-c/blob/master/README.md) and
 * the `tests/` directory in the mean time.
 */

extern crate byteorder;
extern crate protobuf;
extern crate secstr;
extern crate sized_fn;

extern crate signal_protocol_sys as sys;

pub use sized_fn::*;

pub use std::error::Error as Error_;
pub use sys::axolotl::Cipher;
pub use sys::axolotl::LogLevel;
pub use sys::Error as ErrorCode;



/* Utility stuff used everywhere */
#[macro_use]
mod utils;


/* Protocol buffer definitions */
mod protobufs;


/* Public types from the cryptography primitives module */
mod crypto;
pub use crypto::Provider         as CryptoProvider;
pub use crypto::ProviderTemplate as CryptoProviderTemplate;
pub use crypto::{HKDFContext, HMacSha256Provider, KeyPair, PublicKey, PrivateKey};


/* Public types from the fingerprint generator module */
mod fingerprint;
pub use fingerprint::{DisplayableFingerprint, Fingerprint, ScannableFingerprint};


/* Public types from the group cipher and session module */
mod group;
pub use group::{GroupCipher, GroupSessionBuilder};


/* Public types from key management module */
mod keys;
pub use keys::{Address, SenderKeyName, SenderKeyStore};


/* Public types from the protocol serialization and deserialization module */
mod protocol;
pub use protocol::{SenderKeyMessage, SenderKeyDistributionMessage};




/// Error handling type as returned by all crate functions
pub type Result<T> = std::result::Result<T, Error>;

/**
 * Error type used by all crate functions
 */
#[derive(Debug)]
pub struct Error {
	code:        ErrorCode,
	description: String,
	
	cause: Option<Box<std::error::Error>>
}
impl Error {
	/**
	 * Construct a new error object with the given numeric error code (wrapped in an Enum) and
	 * optionally a message
	 */
	pub fn new(code: ErrorCode, message: Option<&str>) -> Self {
		Error {
			code:  code,
			cause: None,
			
			description: match message {
				Some(message) => {
					format!("{:} [Error {:} ({:?})]", message, code as i32, code)
				},
				
				None => {
					format!("Error {:} ({:?})", code as i32, code)
				}
			}
		}
	}
	
	/**
	 * Optionally extend a newly created `Error` object by the given error cause object
	 */
	pub fn with_cause(mut self, cause: Box<std::error::Error>) -> Self {
		self.cause = Some(cause);
		self
	}
	
	/**
	 * Return the error code that can be used to identify this error
	 *
	 * Generally this is the value you will want to use when handling an `Error` in code.
	 */
	#[inline]
	pub fn code(&self) -> ErrorCode {
		self.code
	}
}

impl From<std::io::Error> for Error {
	fn from(error: std::io::Error) -> Self {
		Self::new(ErrorCode::from_io_error(&error), Some(error.description()))
				.with_cause(Box::new(error))
	}
}

impl From<protobuf::ProtobufError> for Error {
	fn from(error: protobuf::ProtobufError) -> Self {
		Self::new(ErrorCode::InvalidProtoBuf, Some(error.description())).with_cause(Box::new(error))
	}
}

impl From<std::string::FromUtf8Error> for Error {
	fn from(error: std::string::FromUtf8Error) -> Self {
		Self::new(ErrorCode::InvalidMessage, Some(error.description())).with_cause(Box::new(error))
	}
}

impl std::error::Error for Error {
	fn description(&self) -> &str {
		&self.description
	}
	
	fn cause(&self) -> Option<&std::error::Error> {
		match self.cause {
			Some(ref cause) => Some(cause.as_ref()),
			None            => None
		}
	}
}

impl std::fmt::Display for Error {
	fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
		fmt.write_str(self.description())
	}
}


/**
 * Serialize and deserialize to and from a `ProtoBuf` binary string
 */
pub trait Serialize<T> where T: protobuf::Message + protobuf::MessageStatic, Self: Sized {
	/**
	 * Create a new instance using the information stored in the given ProtoBuf object
	 */
	#[doc(internal)]
	fn deserialize_protobuf(mut msg: T) -> Result<Self>;
	
	/**
	 * Serialize the contents of this object into the given ProtoBuf type
	 */
	#[doc(internal)]
	fn serialize_protobuf(&self) -> T;
	
	
	
	/**
	 * Deserialize the given byte string into a new instance of this struct
	 */
	fn deserialize(data: &[u8]) -> Result<Self> {
		// Parse the given ProtoBuf message
		Self::deserialize_protobuf(try!(protobuf::parse_from_bytes::<T>(data)))
	}
	
	/**
	 * Serialize the contents of this object into a byte vector
	 */
	fn serialize(&self) -> Vec<u8> {
		self.serialize_protobuf().write_to_bytes().expect("Missing required ProtoBuf fields")
	}
}

/**
 * Serialize and derserialize to and from a `ProtoBuf` binary string using the given
 * `CryptoProvider`
 */
pub trait SerializeCrypto<'cp, T> where T: protobuf::Message + protobuf::MessageStatic, Self:Sized {
	/**
	 * Create a new instance using the information stored in the given ProtoBuf object
	 */
	#[doc(internal)]
	fn deserialize_protobuf(crypto: &'cp CryptoProvider<'cp>, mut msg: T) -> Result<Self>;
	
	/**
	 * Serialize the contents of this object into the given ProtoBuf type
	 */
	#[doc(internal)]
	fn serialize_protobuf(&self) -> T;
	
	
	
	/**
	 * Deserialize the given byte string into a new instance of this struct
	 */
	fn deserialize(crypto: &'cp CryptoProvider<'cp>, data: &[u8]) -> Result<Self> {
		// Parse the given ProtoBuf message
		Self::deserialize_protobuf(crypto, try!(protobuf::parse_from_bytes::<T>(data)))
	}
	
	/**
	 * Serialize the contents of this object into a byte vector
	 */
	fn serialize(&self) -> Vec<u8> {
		self.serialize_protobuf().write_to_bytes().expect("Missing required ProtoBuf fields")
	}
}