use std;

use sized_fn::*;

use ::{Cipher, Result};


mod hkdf;
pub use self::hkdf::Context as HKDFContext;

mod curve;
pub use self::curve::{KeyPair, PublicKey, PrivateKey};


/**
 * Interface of an updatable HMAC-SHA256 crypto implementation
 *
 * The structure implementing this type will be instanciated on the heap using the `new()` method
 * and it's lifetime will be managed by the library. Several instances of a `HMacSha256Provider`
 * structures may therefor exist at the same time, but no single instance will every cross a thread
 * boundary.
 */
pub trait HMacSha256Provider {
	/**
	 * Allocate a new instance of this HMAC-SHA256 generator with the given authentication key
	 */
	fn alloc_new(&self, key: &[u8]) -> Box<HMacSha256Provider>;
	
	/**
	 * Update the underlying SHA256 hash of the HMac with the given input data
	 */
	fn input(&mut self, data: &[u8]);
	
	/**
	 * Return an the given finalized SHA256 key data
	 *
	 * The provider will not be used after this call.
	 */
	fn result(&mut self) -> [u8; 32];
}


/**
 * Data structure that holds references to all callback functions that provide cryptographic
 * services to the application
 */
#[allow(missing_debug_implementations, missing_copy_implementations, type_complexity)]
pub struct ProviderTemplate<'a> {
	/// Callback for obtaining random data (must panic if it cannot obtain any random data)
	pub random:        SizedFn1<'a, &'static mut [u8], ()>,
	/// Callback for calculating the SHA512 over the given data
	pub sha512_digest: SizedFn1<'a, &'static [u8],     [u8; 64]>,
	
	/// Callback for encrypting the given data using AES with the given cipher suit
	pub aes_encrypt: SizedFn4<'a, Cipher, &'static [u8; 32], &'static [u8; 16], &'static [u8], Result<Vec<u8>>>,
	/// Callback for decrypting the given data using AES with the given cipher suit
	pub aes_decrypt: SizedFn4<'a, Cipher, &'static [u8; 32], &'static [u8; 16], &'static [u8], Result<Vec<u8>>>,
	
	/// Reference to an incrementally updatable HMac-SHA256 instance that can spawn more instances
	/// of itself
	pub hmac_sha256: Box<HMacSha256Provider>
}


/**
 * Public interface for using the cryptographic services used by the *signal-protocol* library
 */
pub struct Provider<'a> {
	template: ProviderTemplate<'a>
}
#[allow(unsafe_code)] // Workaround for missing higher-ranked lifetime support for structs
impl<'a> Provider<'a> {
	/**
	 * Create a new instance of this provider with the crypto implementations of the given
	 * provider template
	 */
	pub fn new(template: ProviderTemplate<'a>) -> Provider<'a> {
		Provider {
			template: template
		}
	}
	
	/**
	 * Fill `target` with random data
	 *
	 * # Panics
	 * This will panic if no random data can be obtained for some reason.
	 */
	pub fn random(&self, target: &mut [u8]) {
		//XXX: This `transmute` can hopefully be dropped once higher-ranked lifetimes are supported
		//     for non-traits (https://github.com/rust-lang/rust/issues/33962)
		self.template.random.call(unsafe { std::mem::transmute(target) })
	}
	
	/**
	 * Calculate the SHA512 digest over the given value and return it's binary data as an array
	 */
	pub fn sha512_digest(&self, data: &[u8]) -> [u8; 64] {
		//XXX: This `transmute` can hopefully be dropped once higher-ranked lifetimes are supported
		//     for non-traits (https://github.com/rust-lang/rust/issues/33962)
		self.template.sha512_digest.call(unsafe { std::mem::transmute(data) })
	}
	
	/**
	 * Create and return a new HMac-SHA256 instance with the given authentication key
	 */
	pub fn hmac_sha256_new(&self, key: &[u8]) -> Box<HMacSha256Provider> {
		//XXX: This `transmute` can hopefully be dropped once higher-ranked lifetimes are supported
		//     for non-traits (https://github.com/rust-lang/rust/issues/33962)
		self.template.hmac_sha256.alloc_new(unsafe { std::mem::transmute(key) })
	}
	
	/**
	 * Encrypt the given plaintext using AES256_CTR or AES256_CBC_PKCS5
	 */
	pub fn aes_encrypt(&self, cipher: Cipher, key: &[u8; 32], iv: &[u8; 16], plaintext: &[u8])
			-> Result<Vec<u8>> {
		//XXX: This `transmute` can hopefully be dropped once higher-ranked lifetimes are supported
		//     for non-traits (https://github.com/rust-lang/rust/issues/33962)
		self.template.aes_encrypt.call(cipher,
				unsafe { std::mem::transmute(key)       },
				unsafe { std::mem::transmute(iv)        },
				unsafe { std::mem::transmute(plaintext) })
	}
	
	/**
	 * Decrypt a message previously encrypted with the `aes_encrypt` method
	 */
	pub fn aes_decrypt(&self, cipher: Cipher, key: &[u8; 32], iv: &[u8; 16], ciphertext: &[u8])
			-> Result<Vec<u8>> {
		//XXX: This `transmute` can hopefully be dropped once higher-ranked lifetimes are supported
		//     for non-traits (https://github.com/rust-lang/rust/issues/33962)
		self.template.aes_decrypt.call(cipher,
				unsafe { std::mem::transmute(key)        },
				unsafe { std::mem::transmute(iv)         },
				unsafe { std::mem::transmute(ciphertext) })
	}
}
impl<'a> std::hash::Hash for Provider<'a> {
	fn hash<H>(&self, state: &mut H) where H: std::hash::Hasher {
		std::hash::Hash::hash(&(self.template.hmac_sha256.as_ref() as *const _ as *const usize), state);
	}
}
impl<'a> std::fmt::Debug for Provider<'a> {
	fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(fmt, "crypto::Provider(ptr: {:?})",
		       self.template.hmac_sha256.as_ref() as *const _ as *const usize)
	}
}
impl<'a> Eq for Provider<'a> {}
impl<'a> PartialEq for Provider<'a> {
	fn eq(&self, other: &Provider) -> bool {
		self.template.hmac_sha256.as_ref() as *const _ == other.template.hmac_sha256.as_ref() as *const _
	}
}


pub trait ObtainProvider<'cp> {
	fn get_crypto_provider(&self) -> &'cp Provider<'cp>;
}