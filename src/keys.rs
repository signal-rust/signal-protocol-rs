use protobuf;
use std;

use ::crypto;
use ::protobufs::localstorage as pb;
use ::utils::*;

use ::{Error, ErrorCode, Result, SerializeCrypto};



/**
 * Address of an AXOLOTL message recipient
 */
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Address<'a> {
	/// Unique datum human-readably identifying the given user (usually a phone number)
	pub name: &'a str,
	
	/// Number identifying this particular device of that user
	pub device_id: i32
}
impl<'a> std::fmt::Display for Address<'a> {
	fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(fmt, "\"{:}\" (#{:})", self.name, self.device_id)
	}
}

/**
 * A representation of a (group + sending device) tuple
 */
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct SenderKeyName<'a> {
	/// Human-readable string identifying a group that this user is in (usually it's title)
	pub group_id: &'a str,
	
	/// Unique address of this user
	pub sender: Address<'a>
}
impl<'a> std::fmt::Display for SenderKeyName<'a> {
	fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
		write!(fmt, "Group #{:} from Sender {:}", self.group_id, self.sender)
	}
}


const SENDER_CHAIN_KEY_MESSAGE_KEY_SEED: [u8; 1] = [0x01];
const SENDER_CHAIN_KEY_NEXT_KEY_SEED:    [u8; 1] = [0x02];

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct SenderChainKey<'cp> {
	crypto: &'cp crypto::Provider<'cp>,
	
	iteration: u32,
	seed:      [u8; 32]
}
impl<'cp> SenderChainKey<'cp> {
	pub fn new(crypto: &'cp crypto::Provider<'cp>, iteration: u32, seed: [u8; 32]) -> Self {
		SenderChainKey {
			crypto: crypto,
			
			iteration: iteration,
			seed:      seed
		}
	}
	
	/**
	 * Generate a new chain key instance using random data obtained through the `crypto::Provider`
	 */
	pub fn generate(crypto: &'cp crypto::Provider<'cp>, iteration: u32) -> Self {
		// Generate random seed data
		let mut seed = [0u8; 32];
		crypto.random(&mut seed);
		
		Self::new(crypto, iteration, seed)
	}
	
	/**
	 * Obtain the iteration count of this chain key
	 */
	#[inline]
	pub fn iteration(&self) -> u32 {
		self.iteration
	}
	
	/**
	 * Obtain the seed used to create this chain key
	 */
	#[inline]
	pub fn seed(&self) -> &[u8; 32] {
		&self.seed
	}
	
	pub fn create_message_key(&self) -> SenderMessageKey<'cp> {
		let derivative = {
			let mut hmac_sha256_generator = self.crypto.hmac_sha256_new(&self.seed);
			hmac_sha256_generator.input(&SENDER_CHAIN_KEY_MESSAGE_KEY_SEED);
			hmac_sha256_generator.result()
		};
		
		SenderMessageKey::new(self.crypto, self.iteration, derivative)
	}
	
	pub fn create_next(&self) -> Self {
		let derivative = {
			let mut hmac_sha256_generator = self.crypto.hmac_sha256_new(&self.seed);
			hmac_sha256_generator.input(&SENDER_CHAIN_KEY_NEXT_KEY_SEED);
			hmac_sha256_generator.result()
		};
		
		Self::new(self.crypto, (self.iteration + 1), derivative)
	}
}
impl<'cp> SerializeCrypto<'cp, pb::SenderKeyStateStructure_SenderChainKey> for SenderChainKey<'cp> {
	fn deserialize_protobuf(crypto: &'cp crypto::Provider<'cp>,
	                        msg:    pb::SenderKeyStateStructure_SenderChainKey) -> Result<Self> {
		Ok(Self::new(crypto, msg.get_iteration(), try!(convert_seed_bytes(msg.get_seed()))))
	}
	
	fn serialize_protobuf(&self) -> pb::SenderKeyStateStructure_SenderChainKey {
		let mut msg = pb::SenderKeyStateStructure_SenderChainKey::new();
		msg.set_iteration(self.iteration);
		msg.set_seed((&self.seed[..]).to_owned());
		msg
	}
}




//XXX: Wait for associated constants
const SENDER_MESSAGE_KEY_INFO_MATERIAL: &'static str = "WhisperGroup";

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct SenderMessageKey<'cp> {
	crypto: &'cp crypto::Provider<'cp>,
	
	iteration:  u32,
	cipher_key: [u8; 32],
	iv:         [u8; 16],
	seed:       [u8; 32]
}
impl<'cp> SenderMessageKey<'cp> {
	pub fn new(crypto: &'cp crypto::Provider<'cp>, iteration: u32, seed: [u8; 32]) -> Self {
		let info_material = SENDER_MESSAGE_KEY_INFO_MATERIAL.as_bytes();
		
		let hkdf_context = crypto::HKDFContext::new(crypto, 3);
		let mut derivative = [0u8; 48];
		hkdf_context.derive_secrets(&mut derivative, &seed[..], &[0; 32], Some(info_material));
		
		let mut iv  = [0u8; 16]; (&mut iv[..]) .copy_from_slice(&derivative[0  .. 16]);
		let mut key = [0u8; 32]; (&mut key[..]).copy_from_slice(&derivative[16 .. 48]);
		
		SenderMessageKey {
			crypto: crypto,
			iteration:  iteration,
			cipher_key: key,
			iv:         iv,
			seed:       seed
		}
	}
	
	#[inline]
	pub fn cipher_key(&self) -> &[u8; 32] {
		&self.cipher_key
	}
	
	#[inline]
	pub fn iteration(&self) -> u32 {
		self.iteration
	}
	
	#[inline]
	pub fn iv(&self) -> &[u8; 16] {
		&self.iv
	}
	
	#[inline]
	pub fn seed(&self) -> &[u8; 32] {
		&self.seed
	}
}
impl<'cp> SerializeCrypto<'cp, pb::SenderKeyStateStructure_SenderMessageKey> for SenderMessageKey<'cp> {
	fn deserialize_protobuf(crypto: &'cp crypto::Provider<'cp>,
	                        msg:    pb::SenderKeyStateStructure_SenderMessageKey) -> Result<Self> {
		Ok(Self::new(crypto, msg.get_iteration(), try!(convert_seed_bytes(msg.get_seed()))))
	}
	
	fn serialize_protobuf(&self) -> pb::SenderKeyStateStructure_SenderMessageKey {
		let mut msg = pb::SenderKeyStateStructure_SenderMessageKey::new();
		msg.set_iteration(self.iteration);
		msg.set_seed((&self.seed[..]).to_owned());
		msg
	}
}



#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct SenderKeyState<'cp> {
	crypto: &'cp crypto::Provider<'cp>,
	
	key_id:          u32,
	signing_public:  crypto::PublicKey<'cp>,
	signing_private: Option<crypto::PrivateKey<'cp>>,
	
	/// Current chain key of the sequence
	pub chain_key:    SenderChainKey<'cp>,
	pub message_keys: std::collections::VecDeque<SenderMessageKey<'cp>>
}
impl<'cp> SenderKeyState<'cp> {
	pub fn new(crypto: &'cp crypto::Provider<'cp>,
	           key_id:          u32,
	           chain_key:       SenderChainKey<'cp>,
	           signing_public:  crypto::PublicKey<'cp>,
	           signing_private: Option<crypto::PrivateKey<'cp>>) -> Self {
		SenderKeyState {
			crypto: crypto,
			
			key_id:          key_id,
			signing_public:  signing_public,
			signing_private: signing_private,
			
			chain_key:    chain_key,
			message_keys: std::collections::VecDeque::with_capacity(2000)
		}
	}
	
	/**
	 * Generate a new key state using random data obtained through the `crypto::Provider`
	 */
	pub fn generate(crypto: &'cp crypto::Provider<'cp>) -> Self {
		let key_id      = random_generate_u32(crypto);
		let chain_key   = SenderChainKey::generate(crypto, 0);
		let signing_key = crypto::KeyPair::generate(crypto);
		
		Self::new(crypto, key_id, chain_key, signing_key.public, Some(signing_key.private))
	}
	
	/**
	 * Obtain the (generally) unique ID of this sender key
	 */
	#[inline]
	pub fn key_id(&self) -> u32 {
		self.key_id
	}
	
	/**
	 * Obtain a reference to the public key used to sign this sender key
	 */
	#[inline]
	pub fn signing_public_key(&self) -> &crypto::PublicKey<'cp> {
		&self.signing_public
	}
	
	/**
	 * Maybe obtain a reference to the private key used to sign this sender key's message for
	 * other people
	 */
	#[inline]
	pub fn signing_private_key(&self) -> Option<&crypto::PrivateKey<'cp>> {
		self.signing_private.as_ref()
	}
	
	
	pub fn get_message_key(&mut self, iteration: u32) -> Result<SenderMessageKey<'cp>> {
		if self.chain_key.iteration() > iteration {
			// Chain key is already ahead of this message – Look for pre-generated message
			if let Some(idx) = self.message_keys.iter().position(|mk|mk.iteration() == iteration) {
				return Ok(self.message_keys.remove(idx).unwrap());
			} else {
				let message = &format!("Received message with old counter: {} < {}",
				                       self.chain_key.iteration(), iteration);
				return Err(Error::new(ErrorCode::DuplicateMessage, Some(message)));
			}
		}
		
		// Sanity check: Reject messages that are too far beyond our current chain_key iteration
		if (iteration - self.chain_key.iteration()) > 2000 {
			let message = "Over 2000 lightyears into space";
			return Err(Error::new(ErrorCode::InvalidMessage, Some(message)));
		}
		
		let mut chain_key = self.chain_key.clone();
		while chain_key.iteration() < iteration {
			// Pevent denial-of-service of by making the library store millions at the same time
			if self.message_keys.len() >= 2000 {
				self.message_keys.pop_front();
			}
			
			// Remember message key for this iteration
			// (so that we can later decode intermediate messages that have arrived late)
			let message_key = chain_key.create_message_key();
			self.message_keys.push_back(message_key);
			
			// Move to next chain key iteration
			chain_key = chain_key.create_next();
		}
		
		// Increase chain key once more to make it ready for the next message
		self.chain_key = chain_key.create_next();
		
		// Create and return message key for this iteration
		Ok(chain_key.create_message_key())
	}
}
impl<'cp> SerializeCrypto<'cp, pb::SenderKeyStateStructure> for SenderKeyState<'cp> {
	fn deserialize_protobuf(crypto:  &'cp crypto::Provider<'cp>,
	                        mut msg: pb::SenderKeyStateStructure) -> Result<Self> {
		let signing_public = try!(crypto::PublicKey::from_key_data(crypto,
				try!(convert_pubkey_bytes(msg.get_senderSigningKey().get_public())))
		);
		
		let signing_private =
			if msg.get_senderSigningKey().has_private() {
				Some(try!(crypto::PrivateKey::from_key_data(crypto,
					try!(convert_privkey_bytes(msg.get_senderSigningKey().get_private())))
				))
			} else {
				None
			};
		
		// Create data structure
		let mut result = Self::new(
				crypto,
				msg.get_senderKeyId(),
				try!(SenderChainKey::deserialize_protobuf(crypto, msg.take_senderChainKey())),
				signing_public, signing_private
		);
		
		// Add deserialized message keys
		result.message_keys.reserve(msg.get_senderMessageKeys().len());
		for msg in msg.take_senderMessageKeys().into_iter() {
			result.message_keys.push_back(try!(SenderMessageKey::deserialize_protobuf(crypto,msg)));
		}
		
		Ok(result)
	}
	
	fn serialize_protobuf(&self) -> pb::SenderKeyStateStructure {
		let mut msg = pb::SenderKeyStateStructure::new();
		msg.set_senderKeyId(self.key_id);
		msg.set_senderChainKey(self.chain_key.serialize_protobuf());
		msg.set_senderSigningKey({
			let mut msg = pb::SenderKeyStateStructure_SenderSigningKey::new();
			msg.set_public((&self.signing_public[..]).to_owned());
			
			if let Some(signing_private) = self.signing_private.as_ref() {
				msg.set_private((&signing_private[..]).to_owned());
			}
			
			msg
		});
		msg.set_senderMessageKeys(protobuf::RepeatedField::from_vec(
			self.message_keys.iter().map(|message_key| {
				message_key.serialize_protobuf()
			}
		).collect()));
		msg
	}
}



#[derive(Debug, Clone, PartialEq, Eq)]
pub struct SenderKeyRecord<'cp> {
	crypto: &'cp crypto::Provider<'cp>,
	
	key_states: std::collections::HashMap<u32, SenderKeyState<'cp>>,
	key_state_base: Option<u32>
}
impl<'cp> SenderKeyRecord<'cp> {
	pub fn new(crypto: &'cp crypto::Provider<'cp>) -> Self {
		SenderKeyRecord {
			crypto: crypto,
			
			key_states:     std::collections::HashMap::with_capacity(1),
			key_state_base: None
		}
	}
	
	/**
	 * Does this sender key record contain any key states yet?
	 */
	#[inline]
	pub fn is_empty(&self) -> bool {
		self.key_states.is_empty()
	}
	
	pub fn add_key_state(&mut self, id: u32, iteration: u32, chain_key: [u8; 32],
	                     signature_public: crypto::PublicKey<'cp>) {
		let chain_key = SenderChainKey::new(self.crypto, iteration, chain_key);
		let key_state = SenderKeyState::new(self.crypto, id, chain_key, signature_public, None);
		self.key_states.insert(id, key_state);
	}
	
	pub fn get_key_state(&self) -> Option<&SenderKeyState<'cp>> {
		match self.key_state_base {
			Some(ref id) => self.key_states.get(id),
			None         => None
		}
	}
	
	pub fn get_key_state_mut(&mut self) -> Option<&mut SenderKeyState<'cp>> {
		match self.key_state_base {
			Some(ref id) => self.key_states.get_mut(id),
			None         => None
		}
	}
	
	pub fn get_key_state_by_id(&self, id: u32) -> Option<&SenderKeyState<'cp>> {
		self.key_states.get(&id)
	}
	
	pub fn get_key_state_by_id_mut(&mut self, id: u32) -> Option<&mut SenderKeyState<'cp>> {
		self.key_states.get_mut(&id)
	}
	
	/**
	 * Erase the current sender key state and generate a new key state with the given values
	 *
	 * # Panics
	 *
	 * Panics if the given key state does not contain a private key value.
	 */
	pub fn set_key_state(&mut self, key_state: SenderKeyState<'cp>) {
		let key_id = key_state.key_id();
		assert!(key_state.signing_private_key().is_some());
		
		self.key_states.clear();
		
		self.key_states.insert(key_id, key_state);
		self.key_state_base = Some(key_id);
	}
}
impl<'cp> SerializeCrypto<'cp, pb::SenderKeyRecordStructure> for SenderKeyRecord<'cp> {
	fn deserialize_protobuf(crypto: &'cp crypto::Provider<'cp>,
	                        mut msg: pb::SenderKeyRecordStructure) -> Result<Self> {
		// Create new struct instance
		let mut result = Self::new(crypto);
		
		// Deserialize all `key_states` that had been stored in the ProtoBuf message
		for msg in msg.take_senderKeyStates().into_iter() {
			let key_state = try!(SenderKeyState::deserialize_protobuf(crypto, msg));
			
			// Remember first key state as base key state value
			if result.key_state_base == None {
				result.key_state_base = Some(key_state.key_id());
			}
			
			result.key_states.insert(key_state.key_id(), key_state);
		}
		
		Ok(result)
	}
	
	fn serialize_protobuf(&self) -> pb::SenderKeyRecordStructure {
		let mut msg = pb::SenderKeyRecordStructure::new();
		msg.set_senderKeyStates({
			let mut msg_list = protobuf::RepeatedField::new();
			
			// The base key state must always be the first key state value
			self.key_state_base.as_ref().map(|id| {
				msg_list.push(self.key_states.get(id).unwrap().serialize_protobuf());
			});
			
			// Add all remaining key state values
			for (id, key_state) in &self.key_states {
				if Some(id) != self.key_state_base.as_ref() {
					msg_list.push(key_state.serialize_protobuf());
				}
			}
			
			msg_list
		});
		msg
	}
}


/**
 * Implementation of a storage container for the data related to group messaging
 */
pub trait SenderKeyStore {
	/**
	 * Store a serialized sender key record for a given `(groupId, senderId, deviceId)` tuple
	 *
	 * @param sender_key_name The `(groupId, senderId, deviceId)` tuple
	 * @param record Pointer to a buffer containing the serialized record
	 */
	fn store_sender_key(&mut self, sender_key_name: &SenderKeyName, record: Vec<u8>) -> Result<()>;
	
	/**
	 * Returns a copy of the sender key record corresponding to the `(groupId, senderId, deviceId)`
	 * tuple
	 *
	 * @param sender_key_name The `(groupId, senderId, deviceId)` tuple
	 * @return `Some(…)` if sender key was found, `None` if no sender key was found
	 */
	fn load_sender_key(&self, sender_key_name: &SenderKeyName) -> Result<Option<&Vec<u8>>>;
}