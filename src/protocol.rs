use protobuf;
use std;

use ::{Error, ErrorCode, Result, Serialize, SerializeCrypto};
use ::crypto;
use ::protobufs::whispertext as pb;
use ::utils::*;


const CIPHERTEXT_CURRENT_VERSION:     u8 = 3;
const CIPHERTEXT_UNSUPPORTED_VERSION: u8 = 1;


#[repr(i32)]
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
enum MessageType {
	Signal                = 2,
	Prekey                = 3,
	SenderKey             = 4,
	SenderKeyDistribution = 5
}



fn _deserialize_versioned(data: &[u8], suffix_bytes: usize) -> Result<&[u8]> {
	// Verify that version byte is present
	if data.len() < (1 + suffix_bytes) {
		return Err(Error::new(ErrorCode::EINVAL, Some("Message must not be empty")));
	}
	
	// Check version
	match (data[0] & 0xF0) >> 4 {
		version if version < CIPHERTEXT_CURRENT_VERSION => {
			return Err(Error::new(ErrorCode::LegacyMessage,
			           Some(&format!("Legacy message: {:}", version))))
			;
		},
		version if version > CIPHERTEXT_CURRENT_VERSION => {
			return Err(Error::new(ErrorCode::InvalidVersion,
			           Some(&format!("Unknown version: {:}", version)))
			);
		},
		_ => ()
	}
	
	// Actually parse the ProtoBuf message
	let msg_start = 1;
	let msg_end   = data.len() - suffix_bytes;
	Ok(&data[msg_start .. msg_end])
}
fn deserialize_versioned<T, U>(data: &[u8], suffix_bytes: usize) -> Result<T>
		where T: Serialize<U>, U: protobuf::Message + protobuf::MessageStatic {
	let slice: &[u8] = try!(_deserialize_versioned(data, suffix_bytes));
	T::deserialize_protobuf(try!(protobuf::parse_from_bytes::<U>(slice)))
}
fn deserialize_versioned_crypto<'cp, T, U>(crypto: &'cp crypto::Provider<'cp>,
                                           data: &[u8], suffix_bytes: usize) -> Result<T>
		where T: SerializeCrypto<'cp, U>, U: protobuf::Message + protobuf::MessageStatic {
	let slice: &[u8] = try!(_deserialize_versioned(data, suffix_bytes));
	T::deserialize_protobuf(crypto, try!(protobuf::parse_from_bytes::<U>(slice)))
}


fn serialize_versioned<T, U>(this: &T) -> Vec<u8>
		where T: Serialize<U>, U: protobuf::Message + protobuf::MessageStatic {
	// Generate the ProtoBuf message string
	let mut data =
			this.serialize_protobuf().write_to_bytes().expect("Missing required ProtoBuf fields");
	
	// Add version byte
	data.insert(0, (CIPHERTEXT_CURRENT_VERSION << 4) | CIPHERTEXT_CURRENT_VERSION);
	
	data
}
fn serialize_versioned_crypto<'cp, T, U>(this: &T) -> Vec<u8>
		where T: SerializeCrypto<'cp, U>, U: protobuf::Message + protobuf::MessageStatic {
	// Generate the ProtoBuf message string
	let mut data =
			this.serialize_protobuf().write_to_bytes().expect("Missing required ProtoBuf fields");
	
	// Add version byte
	data.insert(0, (CIPHERTEXT_CURRENT_VERSION << 4) | CIPHERTEXT_CURRENT_VERSION);
	
	data
}



/**
 * Message used to send encrypted data in an one-on-one or group context
 */
#[derive(Eq)]
pub struct SenderKeyMessage {
	message_type: MessageType,
	signature:    [u8; 64],
	
	key_id:       u32,
	iteration:    u32,
	ciphertext:   Vec<u8>
}
impl SenderKeyMessage {
	/**
	 *
	 */
	pub fn new(key_id: u32, iteration: u32, ciphertext: Vec<u8>,
	           signature_key: &crypto::PrivateKey) -> Self {
		// Populate struct
		let mut this = Self::new_unsigned(key_id, iteration, ciphertext);
		
		// Sign struct data with the given public key
		let signature  = signature_key.sign(&serialize_versioned(&this));
		this.signature = signature;
		
		this
	}
	
	#[inline]
	fn new_unsigned(key_id: u32, iteration: u32, ciphertext: Vec<u8>) -> Self {
		SenderKeyMessage {
			message_type: MessageType::SenderKey,
			signature:    [0; 64],
			
			key_id:       key_id,
			iteration:    iteration,
			ciphertext:   ciphertext
		}
	}
	
	/**
	 * Verify that the signature on this message was done by the private key from which the given
	 * public key has been derived
	 */
	pub fn verify_signature(&self, signature_key: &crypto::PublicKey) -> bool {
		signature_key.verify(&serialize_versioned(self), &self.signature)
	}
	
	/**
	 * The encrypted message contents
	 */
	#[inline]
	pub fn ciphertext(&self) -> &[u8] {
		&self.ciphertext
	}
	
	/**
	 * The current message number (that all parties have to agree on)
	 */
	#[inline]
	pub fn iteration(&self) -> u32 {
		self.iteration
	}
	
	/**
	 * The key ID number of the user sending the message
	 */
	#[inline]
	pub fn key_id(&self) -> u32 {
		self.key_id
	}
	
	/**
	 * The cryptographic signature authenticating the sending user
	 */
	#[inline]
	pub fn signature(&self) -> &[u8; 64] {
		&self.signature
	}
}
impl Serialize<pb::SenderKeyMessage> for SenderKeyMessage {
	fn serialize_protobuf(&self) -> pb::SenderKeyMessage {
		let mut msg = pb::SenderKeyMessage::new();
		msg.set_id(self.key_id);
		msg.set_iteration(self.iteration);
		msg.set_ciphertext(self.ciphertext.clone());
		msg
	}
	
	fn deserialize_protobuf(mut msg: pb::SenderKeyMessage) -> Result<Self> {
		if !msg.has_id()
		|| !msg.has_iteration()
		|| !msg.has_ciphertext() {
			return Err(Error::new(ErrorCode::InvalidMessage, Some("Incomplete message")));
		}
		
		Ok(Self::new_unsigned(msg.get_id(), msg.get_iteration(), msg.take_ciphertext()))
	}
	
	
	fn deserialize(msg: &[u8]) -> Result<Self> {
		match deserialize_versioned(msg, 64) as Result<Self> {
			Ok(mut this) => {
				// Copy signature from source message
				let signature_start = msg.len() - 64;
				this.signature = try!(convert_signature_bytes(&msg[signature_start ..]));
				
				Ok(this)
			},
			Err(err) => Err(err)
		}
	}
	
	fn serialize(&self) -> Vec<u8> {
		let mut data = serialize_versioned(self);
		data.extend_from_slice(&self.signature);
		data
	}
}
impl Clone for SenderKeyMessage {
	fn clone(&self) -> Self {
		// Work around the fact that the standard library only implements array with up to 32 items
		SenderKeyMessage {
			message_type: self.message_type.clone(),
			signature:    self.signature,
			
			key_id:     self.key_id,
			iteration:  self.iteration,
			ciphertext: self.ciphertext.clone()
		}
	}
}
impl std::fmt::Debug for SenderKeyMessage {
	fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
		// Work around the fact that the standard library only implements array with up to 32 items
		write!(fmt,
				"PublicKey(message_type: {:?}, signature: {:?}, key_id: {:?}, \
				 iteration: {:?}, ciphertext: {:?})",
				self.message_type, &self.signature as &[u8], self.key_id,
				self.iteration, self.ciphertext
		)
	}
}
impl std::hash::Hash for SenderKeyMessage {
	fn hash<H>(&self, state: &mut H) where H: std::hash::Hasher {
		// Work around the fact that the standard library only implements array with up to 32 items
		state.write_i32(self.message_type.clone() as i32);
		state.write(&self.signature);
		state.write_u32(self.key_id);
		state.write_u32(self.iteration);
		state.write(&self.ciphertext);
	}
}
impl PartialEq for SenderKeyMessage {
	fn eq(&self, other: &Self) -> bool {
		// Work around the fact that the standard library only implements array with up to 32 items
		self.message_type        == other.message_type        &&
		&self.signature as &[u8] == &other.signature as &[u8] &&
		self.key_id              == other.key_id              &&
		self.iteration           == other.iteration           &&
		self.ciphertext          == other.ciphertext
	}
}



/**
 * Message to inform other members of a group about the currently used message key secrets
 */
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct SenderKeyDistributionMessage<'cp> {
	message_type:   MessageType,
	key_id:         u32,
	iteration:      u32,
	chain_key_seed: [u8; 32],
	signature_key:  crypto::PublicKey<'cp>
}
impl<'cp> SenderKeyDistributionMessage<'cp> {
	/**
	 *
	 */
	pub fn new(key_id: u32, iteration: u32, chain_key_seed: [u8; 32],
	           signature_key: crypto::PublicKey<'cp>) -> Self {
		SenderKeyDistributionMessage {
			message_type:   MessageType::SenderKeyDistribution,
			key_id:         key_id,
			iteration:      iteration,
			chain_key_seed: chain_key_seed,
			signature_key:  signature_key
		}
	}
	
	/**
	 * The chain key seed for the current iteration
	 *
	 * A newer chain key cannot be used to construct an older chain key iteration.
	 */
	#[inline]
	pub fn chain_key_seed(&self) -> &[u8; 32] {
		&self.chain_key_seed
	}
	
	/**
	 * The current chain key iteration (first key is 0)
	 */
	#[inline]
	pub fn iteration(&self) -> u32 {
		self.iteration
	}
	
	/**
	 * The key ID number of the user sending the message
	 */
	#[inline]
	pub fn key_id(&self) -> u32 {
		self.key_id
	}
	
	/**
	 * The public signature key of the user sending the message
	 */
	#[inline]
	pub fn signature_key(&self) -> &crypto::PublicKey<'cp> {
		&self.signature_key
	}
}
impl<'cp> SerializeCrypto<'cp, pb::SenderKeyDistributionMessage> for SenderKeyDistributionMessage<'cp> {
	fn serialize_protobuf(&self) -> pb::SenderKeyDistributionMessage {
		let mut msg = pb::SenderKeyDistributionMessage::new();
		msg.set_id(self.key_id);
		msg.set_iteration(self.iteration);
		msg.set_chainKey((&self.chain_key_seed[..]).to_owned());
		msg.set_signingKey((&self.signature_key[..]).to_owned());
		msg
	}
	
	fn deserialize_protobuf(crypto: &'cp crypto::Provider<'cp>,
	                        msg:    pb::SenderKeyDistributionMessage) -> Result<Self> {
		if !msg.has_id()
		|| !msg.has_iteration()
		|| !msg.has_chainKey()
		|| !msg.has_signingKey() {
			return Err(Error::new(ErrorCode::InvalidMessage, Some("Incomplete message")));
		}
		
		let key_data = try!(convert_pubkey_bytes(msg.get_signingKey()));
		Ok(Self::new(
				msg.get_id(),
				msg.get_iteration(),
				try!(convert_chain_key_bytes(msg.get_chainKey())),
				try!(crypto::PublicKey::from_key_data(crypto, key_data))
		))
	}
	
	
	fn deserialize(crypto: &'cp crypto::Provider<'cp>, msg: &[u8]) -> Result<Self> {
		deserialize_versioned_crypto(crypto, msg, 0)
	}
	fn serialize(&self) -> Vec<u8> {
		serialize_versioned_crypto(self)
	}
}