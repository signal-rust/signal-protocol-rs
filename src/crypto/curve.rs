#![allow(len_without_is_empty)] // All `len()` values return hard-coded > 0 values

use secstr;
use std;

use ::crypto;
use ::{Error, ErrorCode, Result};


/**
 * Cryptographical functions used from the C library
 */
extern "C" {
	fn curve25519_donna(mypublic: *mut [u8; 32], secret: *const [u8; 32], basepoint: *const [u8; 32]) -> std::os::raw::c_int;
	fn curve25519_sign(signature_out: *mut [u8; 64], privkey: *const [u8; 32], msg: *const u8, msg_len: std::os::raw::c_long, random: *const [u8; 64]) -> std::os::raw::c_int;
	fn curve25519_verify(signature: *const [u8; 64], pubkey: *const [u8; 32], msg: *const u8, msg_len: std::os::raw::c_long) -> std::os::raw::c_int;
}

const SIGNATURE_LEN: usize = 64;

const PUBLIC_KEY_LEN:         usize = 33;
const PUBLIC_KEY_LEN_HEADER:  usize = 1;
const PUBLIC_KEY_LEN_CONTENT: usize = PUBLIC_KEY_LEN - PUBLIC_KEY_LEN_HEADER;
const PUBLIC_KEY_DATA_TYPE: u8 = 0x05;
const PUBLIC_KEY_BASEPOINT: [u8; 32] = [9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

/**
 * The public key part of an elliptic curve
 */
#[derive(Eq)]
pub struct PublicKey<'cp> {
	crypto: &'cp crypto::Provider<'cp>,
	data: [u8; PUBLIC_KEY_LEN]
}
impl<'cp> PublicKey<'cp> {
	/**
	 * Generate a public key for the given private key
	 */
	#[allow(unsafe_code)] // FFI
	pub fn new(private_key: &PrivateKey<'cp>) -> Self {
		let mut data = [0u8; PUBLIC_KEY_LEN];
		data[0] = PUBLIC_KEY_DATA_TYPE;
		
		let data_ptr = (&mut data[PUBLIC_KEY_LEN_HEADER .. PUBLIC_KEY_LEN]).as_mut_ptr()
		                as *mut [u8; PUBLIC_KEY_LEN_CONTENT];
		let result = unsafe {
			curve25519_donna(data_ptr, private_key.as_ref(), &PUBLIC_KEY_BASEPOINT)
		};
		assert_eq!(result, 0); // C code is hard-coded to only ever `return 0;`
		
		PublicKey {
			crypto: private_key.crypto,
			data:   data
		}
	}
	
	
	/**
	 * Create a new public key data structure from the given key data
	 */
	pub fn from_key_data(crypto:    &'cp crypto::Provider<'cp>,
	                     key_data: [u8; PUBLIC_KEY_LEN]) -> Result<Self> {
		// Validate key data type field
		if key_data[0] != PUBLIC_KEY_DATA_TYPE {
			return Err(Error::new(ErrorCode::InvalidKey,
			           Some(&format!("Invalid key type: {:}", key_data[0])))
			);
		}
		
		Ok(PublicKey {
			crypto: crypto,
			data:   key_data
		})
	}
	
	
	/**
	 * Obtain a reference to the key data (without the type byte)
	 */
	#[inline]
	#[allow(unsafe_code)] // There is no safe way to get an array reference to part of another array
	fn key_data(&self) -> &[u8; PUBLIC_KEY_LEN_CONTENT] {
		unsafe {
			// Obtain raw pointer from key data
			let ptr_src = &self.data as *const [u8; PUBLIC_KEY_LEN] as *const u8;
			
			// Move the pointer forward by one and cast it to a pointer that points only to 32 bytes
			let ptr_dst = ptr_src.offset(PUBLIC_KEY_LEN_HEADER as isize)
			              as *const [u8; PUBLIC_KEY_LEN_CONTENT];
			
			// Convert pointer back into reference
			ptr_dst.as_ref().unwrap()
		}
	}
	
	
	/**
	 * Calculate the cryptographic overlap between this public key and the given private key
	 *
	 * Using the function on a public key obtained through a key exchange, both parties can derive
	 * a shared secrect, without that secret ever being sent across the wire (ECDHE).
	 */
	#[allow(unsafe_code)] // FFI
	pub fn calculate_agreement(&self, private_key: &PrivateKey) -> [u8; PUBLIC_KEY_LEN_CONTENT] {
		let mut overlap = [0; PUBLIC_KEY_LEN_CONTENT];
		
		let result = unsafe {
			curve25519_donna(&mut overlap, private_key.as_ref(), self.key_data())
		};
		assert_eq!(result, 0); // C code is hard-coded to only ever `return 0;`
		
		overlap
	}
	
	
	/**
	 * Obtain the length of the stored public key data
	 */
	#[inline]
	pub fn len(&self) -> usize {
		PUBLIC_KEY_LEN
	}
	
	
	/**
	 * Verify the signature on a (binary) message with this public key
	 */
	#[allow(unsafe_code)] // FFI
	pub fn verify(&self, message: &[u8], signature: &[u8; SIGNATURE_LEN]) -> bool {
		let result = unsafe {
			curve25519_verify(
				signature, self.key_data(), message.as_ptr(), message.len() as std::os::raw::c_long
			)
		};
		
		result == 0
	}
}
impl<'cp> AsRef<[u8; PUBLIC_KEY_LEN]> for PublicKey<'cp> {
	fn as_ref(&self) -> &[u8; PUBLIC_KEY_LEN] {
		&self.data
	}
}
impl<'cp, U> std::ops::Index<U> for PublicKey<'cp> where [u8]: std::ops::Index<U> {
	type Output = <[u8] as std::ops::Index<U>>::Output;
	
	fn index(&self, index: U) -> &Self::Output {
		std::ops::Index::index(&self.data as &[u8], index)
	}
}

impl<'cp> Clone for PublicKey<'cp> {
	fn clone(&self) -> Self {
		// Work around the fact that the standard library only implements array with up to 32 items
		PublicKey {
			crypto: self.crypto,
			data:   self.data
		}
	}
}
impl<'cp> std::fmt::Debug for PublicKey<'cp> {
	fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
		// Work around the fact that the standard library only implements array with up to 32 items
		write!(fmt, "PublicKey(data: {:?})", &self.data as &[u8])
	}
}
impl<'cp> std::hash::Hash for PublicKey<'cp> {
	fn hash<H>(&self, state: &mut H) where H: std::hash::Hasher {
		// Work around the fact that the standard library only implements array with up to 32 items
		state.write(&self.data);
	}
}
impl<'cp> PartialEq for PublicKey<'cp> {
	fn eq(&self, other: &PublicKey) -> bool {
		// Work around the fact that the standard library only implements array with up to 32 items
		&self.data as &[u8] == &other.data as &[u8]
	}
}


const PRIVATE_KEY_LEN: usize = 32;


/**
 * The private key part of an elliptic curve
 */
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct PrivateKey<'cp> {
	crypto: &'cp crypto::Provider<'cp>,
	data:   secstr::SecBox<[u8; PRIVATE_KEY_LEN]>
}
impl<'cp> PrivateKey<'cp> {
	/**
	 * Generate a new private key using the given context
	 */
	pub fn new(crypto: &'cp crypto::Provider<'cp>) -> Self {
		let mut key_data = secstr::SecBox::new(Box::new([0; PRIVATE_KEY_LEN]));
		
		// Fill key data with random data that is trimmed at the recommended bytes
		{
			let key_data_ref = key_data.unsecure_mut();
			crypto.random(key_data_ref);
			(key_data_ref[0])  &= 248;
			(key_data_ref[31]) &= 127;
			(key_data_ref[31]) |= 64;
		}
		
		PrivateKey {
			crypto: crypto,
			data:   key_data
		}
	}
	
	
	/**
	 * Obtain the length of the stored private key data
	 */
	pub fn len(&self) -> usize {
		PRIVATE_KEY_LEN
	}
	
	
	/**
	 * Create a new private key data structure from the given key data
	 */
	pub fn from_key_data(crypto: &'cp crypto::Provider<'cp>,
	                     key_data: Box<[u8; PRIVATE_KEY_LEN]>) -> Result<Self> {
		Ok(PrivateKey {
			crypto: crypto,
			data:   secstr::SecBox::new(key_data)
		})
	}
	
	/**
	 * Sign a (binary) message with this private key
	 */
	#[allow(unsafe_code)] // FFI
	pub fn sign(&self, message: &[u8]) -> [u8; SIGNATURE_LEN] {
		// Generate some random data
		let mut random = [0; 64];
		self.crypto.random(&mut random);
		
		let mut signature = [0; 64];
		let result = unsafe {
			curve25519_sign(
				&mut signature,
				self.data.unsecure(),
				message.as_ptr(),
				message.len() as _,
				&random
			)
		};
		assert_eq!(result, 0); // Can only fail with OOM
		
		signature
	}
}
impl<'cp> AsRef<[u8; PRIVATE_KEY_LEN]> for PrivateKey<'cp> {
	fn as_ref(&self) -> &[u8; PRIVATE_KEY_LEN] {
		self.data.unsecure()
	}
}
impl<'cp, U> std::ops::Index<U> for PrivateKey<'cp> where [u8]: std::ops::Index<U> {
	type Output = <[u8] as std::ops::Index<U>>::Output;
	
	fn index(&self, index: U) -> &Self::Output {
		std::ops::Index::index(self.data.unsecure() as &[u8], index)
	}
}
impl<'cp> std::hash::Hash for PrivateKey<'cp> {
	fn hash<H>(&self, state: &mut H) where H: std::hash::Hasher {
		std::hash::Hash::hash(self.crypto, state);
		std::hash::Hash::hash(self.data.unsecure() as &[u8], state);
	}
}



/**
 * A combined storage container for a public/private key pair
 * 
 * The stored public key must always be a derivative of the stored private key.
 */
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct KeyPair<'cp> {
	/// Public key derivated from the private key
	pub public:  PublicKey<'cp>,
	/// Private key
	pub private: PrivateKey<'cp>
}
impl<'cp> KeyPair<'cp> {
	/**
	 * Create a new key pair from an existing public and private key
	 */
	pub fn new(public_key: PublicKey<'cp>, private_key: PrivateKey<'cp>) -> Self {
		KeyPair {
			public:  public_key,
			private: private_key
		}
	}
	
	/**
	 * Generate a private key and derive a public key from it
	 */
	pub fn generate(crypto: &'cp crypto::Provider<'cp>) -> Self {
		// Generate private key
		let private_key = PrivateKey::new(crypto);
		
		// Derive public key from private key
		let public_key = PublicKey::new(&private_key);
		
		// Create data structure from this
		Self::new(public_key, private_key)
	}
}