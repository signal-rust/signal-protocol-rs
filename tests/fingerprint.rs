mod common;
use common::*;


const ALICE_PUBLIC: [u8; 33] = [
		0x05, 0xBB, 0x9D, 0xAD, 0xC3, 0xF2, 0x91,
		0x72, 0x6F, 0x91, 0xB7, 0x64, 0xA0, 0x2D,
		0x9A, 0x2C, 0x3A, 0x3C, 0xD0, 0xE1, 0x3D,
		0x6A, 0x52, 0x70, 0x88, 0x9A, 0x65, 0xE7,
		0x17, 0xF5, 0xDB, 0xE5, 0x17];


const BOB_PUBLIC: [u8; 33] = [
		0x05, 0x20, 0x83, 0x88, 0xDC, 0xF7, 0x23,
		0x68, 0xAA, 0xF7, 0x87, 0xC3, 0xF5, 0xD0,
		0x08, 0xAF, 0x3D, 0xFC, 0xB0, 0x20, 0xC3,
		0xF6, 0x81, 0xC2, 0x84, 0x51, 0x1C, 0x23,
		0xB8, 0x16, 0x71, 0x50, 0x05];



#[test]
fn test_scannable_fingerprint_serialize() {
	let crypto = make_crypto_provider();
	
	// Generate a keypair for Alice and Bob
	let alice = signal::KeyPair::generate(&crypto);
	let bob   = signal::KeyPair::generate(&crypto);
	
	// Generate fingerprints for Alice's and Bob's public key
	let alice_fingerprint = signal::ScannableFingerprint::new(1,
			"+14152222222".to_owned(), alice.public.clone(),
			"+14153333333".to_owned(), bob.public.clone());
	let bob_fingerprint = signal::ScannableFingerprint::new(1,
			"+14153333333".to_owned(), bob.public,
			"+14152222222".to_owned(), alice.public);
	
	assert!(alice_fingerprint.eq_complementary(&bob_fingerprint));
	
	// Serialize and deserialize Bob's fingerprint and check the result
	let bob_data = bob_fingerprint.serialize();
	let bob_deserialized = signal::ScannableFingerprint::deserialize(&crypto, &bob_data).unwrap();
	assert!(alice_fingerprint.eq_complementary(&bob_deserialized));
}

#[test]
fn test_expected_fingerprints() {
	let crypto = make_crypto_provider();
	
	const TEXT:  &'static str = "280453495159653131690525187060866674650885177562699606988867";
	const BYTES: [u8; 104]    = [
		0x08, 0x00, 0x12, 0x31, 0x0A, 0x21, 0x05, 0xBB, 0x9D, 0xAD, 0xC3,
		0xF2, 0x91, 0x72, 0x6F, 0x91, 0xB7, 0x64, 0xA0, 0x2D, 0x9A, 0x2C,
		0x3A, 0x3C, 0xD0, 0xE1, 0x3D, 0x6A, 0x52, 0x70, 0x88, 0x9A, 0x65,
		0xE7, 0x17, 0xF5, 0xDB, 0xE5, 0x17, 0x12, 0x0C, 0x2B, 0x31, 0x34,
		0x31, 0x35, 0x32, 0x32, 0x32, 0x32, 0x32, 0x32, 0x32, 0x1A, 0x31,
		0x0A, 0x21, 0x05, 0x20, 0x83, 0x88, 0xDC, 0xF7, 0x23, 0x68, 0xAA,
		0xF7, 0x87, 0xC3, 0xF5, 0xD0, 0x08, 0xAF, 0x3D, 0xFC, 0xB0, 0x20,
		0xC3, 0xF6, 0x81, 0xC2, 0x84, 0x51, 0x1C, 0x23, 0xB8, 0x16, 0x71,
		0x50, 0x05, 0x12, 0x0C, 0x2B, 0x31, 0x34, 0x31, 0x35, 0x33, 0x33,
		0x33, 0x33, 0x33, 0x33, 0x33
	];
	
	// Decode Alice's and Bob's public key
	let alice_public = signal::PublicKey::from_key_data(&crypto, ALICE_PUBLIC).unwrap();
	let bob_public   = signal::PublicKey::from_key_data(&crypto, BOB_PUBLIC).unwrap();
	
	let alice_fingerprint = signal::Fingerprint::generate(&crypto, 1024,
			"+14152222222".to_owned(), alice_public,
			"+14153333333".to_owned(), bob_public
	);
	
	assert_eq!(alice_fingerprint.displayable().text(), TEXT);
	assert_eq!(alice_fingerprint.scannable().serialize().as_ref() as &[u8], &BYTES as &[u8]);
}

#[test]
fn test_matching_fingerprints() {
	let crypto = make_crypto_provider();
	
	// Generate an identity for Alice and Bob
	let alice = signal::KeyPair::generate(&crypto);
	let bob   = signal::KeyPair::generate(&crypto);
	
	// Generate fingerprints for Alice's and Bob's public key
	let alice_fingerprint = signal::Fingerprint::generate(&crypto, 1024,
			"+14152222222".to_owned(), alice.public.clone(),
			"+14153333333".to_owned(), bob.public.clone()
	);
	let bob_fingerprint = signal::Fingerprint::generate(&crypto, 1024,
			"+14153333333".to_owned(), bob.public,
			"+14152222222".to_owned(), alice.public
	);
	
	// Verify that the fingerprint text matches
	assert_eq!(alice_fingerprint.displayable().text(), bob_fingerprint.displayable().text());
	assert_eq!(alice_fingerprint.displayable().text().len(), 60);
	
	// Verify that the fingerprint values complement each other
	assert!(alice_fingerprint.scannable().eq_complementary(bob_fingerprint.scannable()));
	assert!(bob_fingerprint.scannable().eq_complementary(alice_fingerprint.scannable()));
}

#[test]
fn test_mismatching_fingerprints() {
	let crypto = make_crypto_provider();
	
	// Generate an identity for Alice, Bob and "the other guy"
	let alice = signal::KeyPair::generate(&crypto);
	let bob   = signal::KeyPair::generate(&crypto);
	let mitm  = signal::KeyPair::generate(&crypto);
	
	// Generate fingerprints for Alice's and Bob's public key (but with Bob's bigger brother…)
	let alice_fingerprint = signal::Fingerprint::generate(&crypto, 1024,
			"+14152222222".to_owned(), alice.public.clone(),
			"+14153333333".to_owned(), mitm.public
	);
	let bob_fingerprint = signal::Fingerprint::generate(&crypto, 1024,
			"+14153333333".to_owned(), bob.public,
			"+14152222222".to_owned(), alice.public
	);
	
	// Verify that the fingerprint text doesn't match
	assert!(alice_fingerprint.displayable().text() != bob_fingerprint.displayable().text());
	
	// Verify that the fingerprint values do not complement each other
	assert!(!alice_fingerprint.scannable().eq_complementary(bob_fingerprint.scannable()));
	assert!(!bob_fingerprint.scannable().eq_complementary(alice_fingerprint.scannable()));
}

#[test]
fn test_mismatching_identifiers() {
	let crypto = make_crypto_provider();
	
	// Generate an identity for Alice and Bob
	let alice = signal::KeyPair::generate(&crypto);
	let bob   = signal::KeyPair::generate(&crypto);
	
	// Generate fingerprints for Alice's and Bob's public key
	// (this time with a typo in Bob's phone number)
	let alice_fingerprint = signal::Fingerprint::generate(&crypto, 1024,
			"+14152222222".to_owned(), alice.public.clone(),
			"+14153333334".to_owned(), bob.public.clone()
	);
	let bob_fingerprint = signal::Fingerprint::generate(&crypto, 1024,
			"+14153333333".to_owned(), bob.public,
			"+14152222222".to_owned(), alice.public
	);
	
	// Verify that the fingerprint text doesn't match
	assert!(alice_fingerprint.displayable().text() != bob_fingerprint.displayable().text());
	
	// Verify that the fingerprint values do not complement each other
	assert!(!alice_fingerprint.scannable().eq_complementary(bob_fingerprint.scannable()));
	assert!(!bob_fingerprint.scannable().eq_complementary(alice_fingerprint.scannable()));
}